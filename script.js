const btnIp = document.createElement('button');
const container = document.createElement('div');
const wrapper = document.createElement('div');
btnIp.classList.add('btn');
container.classList.add('container');
wrapper.classList.add('wrapper');
btnIp.innerText = `Знайти по IP`;

document.body.append(container);
container.append(btnIp);
container.append(wrapper);


render = (ipObj, ipValue, continent) => {

  const ipNum = document.createElement('h5');
  const timeZone = document.createElement('p');
  const countryName = document.createElement('p');
  const cityName = document.createElement('p');
  const regionName = document.createElement('p');
  const continentName = document.createElement('p');


  ipNum.innerHTML = `IP: ${ipValue}`;
  timeZone.innerHTML = `Time zone: ${ipObj.timezone}`;
  continentName.innerHTML = `Continent: ${continent}`;
  countryName.innerHTML = ` Country: ${ipObj.country}`;
  cityName.innerHTML = ` City: ${ipObj.city}`;
  regionName.innerHTML = ` Region name: ${ipObj.regionName}`;


  wrapper.append(ipNum);
  wrapper.append(timeZone);
  wrapper.append(continentName);
  wrapper.append(countryName);
  wrapper.append(regionName);
  wrapper.append(cityName);

}

const fetchRequest = async () => {
  try {
    const apiKey = '4df516ffb202400389dc4e925f4ec448'
    const getResponse = await fetch('https://api.ipify.org/?format=json');
    const ip = await getResponse.json();
    const ipValue = ip.ip;
    const sendResponse = await fetch(`http://ip-api.com/json/${ip.ip}`, {
      method: 'POST'
    });
    const ipObj = await sendResponse.json();
    await fetch(`https://api.opencagedata.com/geocode/v1/json?q=${ipObj.lat}+${ipObj.lon}&key=${apiKey}`)
      .then(response => response.json())
      .then(data => {
        const continent = data.results[0]?.components.continent;
        render(ipObj, ipValue, continent);
      })

  } catch (err) {
    console.log(err);
  }
}
btnIp.addEventListener('click', fetchRequest);

